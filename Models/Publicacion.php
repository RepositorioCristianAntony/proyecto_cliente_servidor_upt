<?php

namespace Clase3\Models;

class Publicacion extends Conexion
{
    public $id;
    public $contenido;
    public $fecha_hora;
    public $usuario_id;
    public $usuario;

    static function all()
    {
        $me = new Conexion();
        $pre = mysqli_prepare($me->con, "SELECT * FROM publicaciones");
        $pre->execute();
        $res = $pre->get_result();

        $publicaciones = [];
        while ($publicacion = $res->fetch_object(Publicacion::class)){
            array_push($publicaciones, $publicacion);
        }


        return $publicaciones;
    }
    static function find($id)
    {
        $me = new Conexion();
        $pre = mysqli_prepare($me->con, "SELECT * FROM publicaciones WHERE id = ?");
        $pre->bind_param("i", $id);
        $pre->execute();
        $res = $pre->get_result();

        return $res->fetch_object(Publicacion::class);
    }

    function insert()
    {
        $this->fecha_hora = date("Y-m-d");
        $pre = mysqli_prepare($this->con, "INSERT INTO publicaciones(usuario_id,contenido,fecha_hora)
        VALUES(?,?,?)");
        $pre->bind_param("iss", $this->usuario_id, $this->contenido, $this->fecha_hora);
        $pre->execute();
        $pre_ = mysqli_prepare($this->con, "SELECT LAST_INSERT_ID() id");
        $pre_->execute();
        $r = $pre_->get_result();
        $this->id = $r->fetch_assoc()["id"];
        return true;
    }

    function update(){
        $this->fecha_hora = date("Y-m-d");
        $pre = mysqli_prepare($this->con, "UPDATE publicaciones set contenido=?,fecha_hora=? WHERE id = ?");
        $pre->bind_param("ssi", $this->contenido, $this->fecha_hora, $this->id);
        $pre->execute();
        return true;
    }

    function usuario(){
        $usa = Usuario::find($this->usuario_id);
        return $usa;
    }

}