<?php
namespace Clase3\Models;

class Usuario extends Conexion
{
    public $id;
    public $nombre;
    public $apellidos;
    public $usuario;
    public $password;
    public $fecha_registro;
    public $foto_perfil;
    public $fecha_nacimiento;
    public $correo;
    public $telefono;

    static function selectAll()
    {
        $me = new Conexion();
        $pre = mysqli_prepare($me->con, "SELECT * FROM usuarios");
        $pre->execute();
        $res = $pre->get_result();

        $usuarios = [];
        while ($usuario = $res->fetch_object(Usuario::class))
            array_push($usuarios, $usuario);


        return $usuarios;
    }

    static function find($id)
    {
        $me = new Conexion();
        $pre = mysqli_prepare($me->con, "SELECT * FROM usuarios WHERE id = ?");
        $pre->bind_param("i", $id);
        $pre->execute();
        $res = $pre->get_result();

        return $res->fetch_object(Usuario::class);
    }
    static function findByEmail($email){
        $me = new Conexion();
        $pre = mysqli_prepare($me->con, "SELECT * FROM usuarios where correo=?");
        $pre->bind_param("s", $email);
        $pre->execute();
        $re = $pre->get_result();

        return $re->fetch_object(Usuario::class);
    }

    function nombreCompleto()
    {
        return $this->nombre . " " . $this->apellidos;
    }


    function insert()
    {
        $this->fecha_registro = date("Y-m-d");
        $pre = mysqli_prepare($this->con, "INSERT INTO usuarios(nombre,apellidos,usuario,password,fecha_registro,foto_perfil,fecha_nacimiento,correo,telefono)
        VALUES(?,?,?,?,?,?,?,?,?)");
        $pre->bind_param("sssssssss", $this->nombre, $this->apellidos, $this->usuario, $this->password, $this->fecha_registro,$this->foto_perfil, $this->fecha_nacimiento, $this->correo, $this->telefono);
        $pre->execute();
        $pre_ = mysqli_prepare($this->con, "SELECT LAST_INSERT_ID() id");
        $pre_->execute();
        $r = $pre_->get_result();
        $this->id = $r->fetch_assoc()["id"];
        return true;
    }

    function update(){
        $pre = mysqli_prepare($this->con, "UPDATE usuarios set nombre=?,apellidos=?,usuario=?,password=?,fecha_nacimiento=?,correo=?,telefono=?,foto_perfil=? WHERE id = ?");
        $pre->bind_param("ssssssssi", $this->nombre, $this->apellidos, $this->usuario, $this->password, $this->fecha_nacimiento, $this->correo, $this->telefono,$this->foto_perfil, $this->id);
        $pre->execute();
        return true;
    }
}