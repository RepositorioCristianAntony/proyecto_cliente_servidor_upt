<?php
use Clase3\Models\Usuario;

include "Models/Conexion.php";
include "Models/Usuario.php";

class UsuariosController
{
    function __construct()
    {
        if ($_GET["action"] != "login" && $_GET["action"] != "doLogin" && $_GET["action"] != "create" && $_GET["action"] != "testEncriptar") {
            if (!isset($_SESSION["usuario"])) {
                // Sesión no ha sido iniciada
                $_SESSION["flash"] = "No ha iniciado sesión";

                header("Location:/curso_desarrollo/Curso3/Clase3/?controller=Usuarios&action=login");
            }
        }
    }

    public function testEncriptar()
    {
        echo password_hash("123", PASSWORD_DEFAULT);
    }

    // action index
    public function index()
    {
        $idUsuario = $_SESSION["usuario"];
        // Sesión está iniciada correctamente
        $usuario = Usuario::find($idUsuario);

        require_once "Views/usuarios/index.php";
    }

    public function create()
    {
        if (isset($_POST)) {
            if (getimagesize($_FILES["foto"]["tmp_name"])) {
                $name = $_FILES["foto"]["name"];
                $directorio = "images/";

                $usuario = new Usuario();
                $usuario->nombre = $_POST["nombre"];
                $usuario->apellidos = $_POST["apellidos"];
                $usuario->usuario = $_POST["usuario"];
                $usuario->correo = $_POST["correo"];
                $usuario->foto_perfil = "hola.png";
                $usuario->fecha_registro = date("Y-m-d");
                $usuario->password = password_hash($_POST["password"], PASSWORD_BCRYPT);

                $usuario->insert();
                mkdir($directorio . $usuario->id);
                move_uploaded_file($_FILES["foto"]["tmp_name"], $directorio . $usuario->id . "/" . $name);
                $usuario->foto_perfil = $directorio . $usuario->id . "/" . $name;
                $usuario->update();
                $_SESSION["flash"] = "Cuenta creada correctamente. Inicie sesión ahora.";
                header("Location:/curso_desarrollo/Curso3/Clase3/?controller=Usuarios&action=login");
            } else {
                $_SESSION["flash"] = "Adjunte una foto de perfil para continuar.";
                header("Location:/curso_desarrollo/Curso3/Clase3/?controller=Usuarios&action=login");
            }
        }
    }

    public function login()
    {
        require_once "Views/usuarios/login.php";
    }

    public function doLogin()
    {
        if (isset($_POST)) {
            $usuario = Usuario::findByEmail($_POST["correo"]);
            if ($usuario == NULL) {
                $_SESSION["flash"] = "Usuario incorrecto";
                header("Location:/curso_desarrollo/Curso3/Clase3/?controller=Usuarios&action=login");
            } else {
                if (password_verify($_POST["password"], $usuario->password)) {
                    $_SESSION["usuario"] = $usuario->id;
                    header("Location:/curso_desarrollo/Curso3/Clase3/?controller=Usuarios&action=index");
                } else {
                    $_SESSION["flash"] = "Contraseña incorrecta";
                    header("Location:/curso_desarrollo/Curso3/Clase3/?controller=Usuarios&action=login");
                }
            }
        }
    }
}