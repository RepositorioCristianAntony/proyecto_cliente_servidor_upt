<?php
use Clase3\Models\Publicacion;
use Clase3\Models\Usuario;

include "Models/Conexion.php";
include "Models/Publicacion.php";
include "Models/Usuario.php";

class PublicacionesController
{
    function __construct()
    {
        if ($_GET["action"] != "login" && $_GET["action"] != "doLogin" && $_GET["action"] != "create" && $_GET["action"] != "testEncriptar") {
            if (!isset($_SESSION["usuario"])) {
                // Sesión no ha sido iniciada
                $_SESSION["flash"] = "No ha iniciado sesión";
                header("Location:/curso_desarrollo/Curso3/Clase3/?controller=Usuarios&action=login");
            }
        }
    }

    // action index
    public function index()
    {
        $idUsuario = $_SESSION["usuario"];
        // Sesión está iniciada correctamente
        $usuario = Usuario::find($idUsuario);

        //require_once "Views/usuarios/index.php";
        echo "Index de publicaciones";
    }

    public function createView()
    {
        $idUsuario = $_SESSION["usuario"];
        // Sesión está iniciada correctamente
        $usuario = Usuario::find($idUsuario);

        require_once "Views/publicaciones/create.php";
    }

    public function create()
    {
        if (isset($_POST)) {
            if (getimagesize($_FILES["foto"]["tmp_name"])) {

                $directorio = "images/";

                $publicacion = new Publicacion();
                $publicacion->contenido = $_POST["contenido"];
                $publicacion->foto = "publicacion.png";
                $publicacion->fecha_hora = date("Y-m-d");
                $publicacion->usuario_id = $_POST["idUsuario"];

                $name = "publicacion_".$_FILES["foto"]["name"];

                mkdir($directorio . $_POST["idUsuario"]."/publicaciones");
                move_uploaded_file($_FILES["foto"]["tmp_name"], $directorio . $_POST["idUsuario"] . "/publicaciones/" . $name);
                $publicacion->foto = $directorio . $_POST["idUsuario"] . "/" . $name;
                $publicacion->insert();
                $_SESSION["flash"] = "¡Publicación creada!.";
                //header("Location:/curso_desarrollo/Curso3/Clase3/?controller=Publicaciones&action=index");
            } else {
                $_SESSION["flash"] = "Adjunte una foto de la publicación para continuar.";
                header("Location:/curso_desarrollo/Curso3/Clase3/?controller=Publicaciones&action=createView");
            }
        }
    }
}