<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        input[name="foto"] {
            display: none;
        }

        .uploader {
            width: 250px;
            height: 250px;
            border: dashed 1px #000;
            cursor: pointer;
        }
    </style>
</head>
<body>
<div class="container">
    <?php if (isset($_SESSION["flash"])) { ?>
        <span class="alert alert-danger">
            <?php
            echo $_SESSION["flash"];
            unset($_SESSION["flash"]);
            ?>
        </span>
    <?php } ?>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
               aria-selected="true">Login</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
               aria-controls="profile" aria-selected="false">Registro</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
            <!--login-->
            <form action="/curso_desarrollo/Curso3/Clase3/?controller=Usuarios&action=doLogin" method="Get" id="login">
                <div class="form-group">
                    <label for="">Correo</label>
                    <input type="email" name="correo" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Contraseña</label>
                    <input type="password" name="password" class="form-control">
                </div>
                <input type="submit" value="Ingresar" class="btn btn-success">
            </form>
        </div>
        <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <!--registro-->
            <form action="/curso_desarrollo/Curso3/Clase3/?controller=Usuarios&action=create" enctype="multipart/form-data" method="POST"
                  id="registro">
                <div class="form-group">
                    <label for="">Nombre</label>
                    <input type="text" name="nombre" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Apellidos</label>
                    <input type="text" name="apellidos" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Usuario</label>
                    <input type="text" name="usuario" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Correo</label>
                    <input type="email" name="correo" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Contraseña</label>
                    <input type="password" name="password" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Foto de perfil</label>
                    <div class="uploader">
                        <img src="">
                    </div>
                    <input type="file" name="foto" accept="image/*">
                </div>
                <input type="submit" value="Ingresar" class="btn btn-success">
            </form>
        </div>
    </div>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $("#login [type='submit']").on("click", function (e) {
            e.preventDefault();
            var correo = $("#login input[name='correo']");
            var password = $("#login input[name='password']");

            if (correo.val() == "" || password.val() == "") {
                alert("Debe llenar el formulario");
                return;
            }

            $("#login").submit();
        });
        $(".uploader").on("click", function () {
            $("input[name='foto']").trigger("click");
        });
        $("input[name='foto']").on("change", function () {
            var file = $(this).prop("files")[0];
            var fileReader = new FileReader();
            fileReader.readAsDataURL(file);
            fileReader.onload = function () {
                var base64 = fileReader.result;
                var image = new Image();
                image.src = base64;
                image.onload = function () {
                    if (image.width <= 250 && image.height <= 250) {
                        $(".uploader img").attr("src", base64);
                    } else {
                        alert("La imagen debe ser de 100 x 100.");
                    }
                };
            };
        });
    });
</script>
</html>