<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Crear Publicación</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        input[name="foto"] {
            display: none;
        }

        .uploader {
            width: 250px;
            height: 250px;
            border: dashed 1px #000;
            cursor: pointer;
        }
    </style>
</head>
<body>
<div class="container">
    <?php if (isset($_SESSION["flash"])) { ?>
        <span class="alert alert-danger">
            <?php
            echo $_SESSION["flash"];
            unset($_SESSION["flash"]);
            ?>
        </span>
    <?php } ?>
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <form action="/curso_desarrollo/Curso3/Clase3/?controller=Publicaciones&action=create" enctype="multipart/form-data" method="POST" id="form-publicacion">
                <input type="hidden" name="idUsuario" value="<?php echo $usuario->id;?>">
                <div class="form-group">
                    <label>Contenido</label>
                    <textarea name="contenido" class="form-control"></textarea>
                </div>

                <div class="form-group">
                    <label for="">Foto de perfil</label>
                    <div class="uploader">
                        <img src="">
                    </div>
                    <input type="file" name="foto" accept="image/*">
                </div>
                <input type="submit" value="Ingresar" class="btn btn-success">

            </form>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $("#form-publicacion [type='submit']").on("click", function (e) {
            e.preventDefault();
            var contenido = $("#login input[name='contenido']");

            if (contenido.val() == "") {
                alert("Debe llenar el formulario");
                return;
            }

            $("#form-publicacion").submit();
        });
        $(".uploader").on("click", function () {
            $("input[name='foto']").trigger("click");
        });
        $("input[name='foto']").on("change", function () {
            var file = $(this).prop("files")[0];
            var fileReader = new FileReader();
            fileReader.readAsDataURL(file);
            fileReader.onload = function () {
                var base64 = fileReader.result;
                var image = new Image();
                image.src = base64;
                image.onload = function () {
                    if (image.width <= 250 && image.height <= 250) {
                        $(".uploader img").attr("src", base64);
                    } else {
                        alert("La imagen debe ser de 100 x 100.");
                    }
                };
            };
        });
    });
</script>
</body>
</html>